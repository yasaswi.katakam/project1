import math


class Calculator:

    @staticmethod
    def circle(r):
        ac = math.pi * (r ** 2)
        pc = 2 * math.pi * r
        res1 = (ac, pc)
        return res1

    @staticmethod
    def rectangle(lr, b):
        ar = lr * b
        pr = 2 * (lr + b)
        res2 = (ar, pr)
        return res2

    @staticmethod
    def triangle(bt, ht, lt):
        a_triangle = 0.5 * bt * ht
        p_triangle = bt * ht * lt
        res3 = (a_triangle, p_triangle)
        return res3

    def square(ls):
        a_square = ls * ls
        p_square = 4 * ls
        res4 = (a_square, p_square)
        return res4