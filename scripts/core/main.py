from scripts.core.calculate import Calculator
obj = Calculator()
shape = input("Enter shape")
if shape == "circle":
    r = int(input("Enter radius"))
    ans = obj.circle(r)
    print(ans)
elif shape == "rectangle":
    le = int(input("Enter length"))
    b = int(input("Enter breadth"))
    ans = obj.rectangle(le, b)
    print(ans)
elif shape == "triangle":
    b = int(input("Enter base:"))
    h = int(input("Enter height:"))
    l2 = int(input("Enter length:"))
    ans = obj.triangle(b, h, l2)
    print(ans)
elif shape == "square":
    ls = int(input("Enter length:"))
    ans1 = obj.square(ls)
    print(ans1)